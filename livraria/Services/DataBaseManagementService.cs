using livraria.Models;
using Microsoft.EntityFrameworkCore;

namespace livraria.Services;

public static class DataBaseManagementService
{
    public static void MigrationInitialize(this IApplicationBuilder app)
    {
        using(var serviceScope = app.ApplicationServices.CreateScope())
        {
            var serviceDb = serviceScope.ServiceProvider
                            .GetService<AppDbContext>();
            
            // vai aplicar qualquer migração pendente
            serviceDb?.Database.Migrate();
        }
    }
}